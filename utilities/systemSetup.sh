sudo yum install -y httpd conda php
scriptPath=$(realpath $0)
srcPath=$(dirname $scriptPath)
srcPath=$(dirname $srcPath)
username=$(whoami)

conda env create -f $srcPath/ess-viewer.yml
conda init bash
a=$(cat ~/.bashrc | grep 'conda activate ess')
if [ ${#a} -eq 0 ]; then
	echo "conda activate ess" >> ~/.bashrc
fi
