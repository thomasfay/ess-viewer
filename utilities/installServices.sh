installPath=/var/www/html/ess-viewer
scriptPath=$(realpath $0)
srcPath=$(dirname $scriptPath)
srcPath=$(dirname $srcPath)
username=$(whoami)

# Services
servInstallPath=/etc/systemd/system
servSrcPath=$srcPath/back/services
hostname=$(hostname)
for el in $(ls $servSrcPath)
do
   eval "sed -e 's/{{ hostname }}/$hostname/' \
       -e 's/{{ user }}/$username/g' \
	< $servSrcPath/$el" | sudo tee $servInstallPath/$el > /dev/null
done
sudo systemctl daemon-reload

for el in $(ls $servSrcPath)
do
    sudo systemctl enable $el
done
# HTTP conf
installWebConf=/etc/httpd/conf
sudo cp $srcPath/utilities/httpd.conf $installWebConf
# HTTP conf
installWebConf=/etc/httpd/conf
sudo cp $srcPath/utilities/httpd.conf $installWebConf
