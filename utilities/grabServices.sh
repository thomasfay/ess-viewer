installPath=/etc/systemd/system
sourcePath=/home/thomasfay/ess-viewer/back/services

cp $installPath/ess-viewer.service $sourcePath
cp $installPath/device-viewer.service $sourcePath
cp $installPath/host-viewer.service $sourcePath
cp $installPath/pv-viewer.service $sourcePath
cp $installPath/lbs-viewer.service $sourcePath
cp $installPath/cable-viewer.service $sourcePath
