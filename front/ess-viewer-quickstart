<DOCTYPE html>
<html>
<head>
  <title>ESS Viewer Quickstart</title>
  <meta charset="UTF-8">
  <style>
  body {
    background-color: ivory;
  }
  </style>
</head>
<h1>Main Query Box</h1>
Provides the possibility to retrieve FBS listings together with associated additional meta-data options such as ESS Name. Queries are case-insensitive and the leading '=ESS' is optional, as is the leading "=" character. The results are presented in a listing that can be further navigated with mouse-clicks. The output listing can also be saved to file.<br>
See the following examples below:
<ul>
<li>=ESS.ACC</li>
<li>ESS.ACC</li>
<li>acc.a02</li>
<li>ts.cup</li>
</ul>
<h2>Advanced Queries</h2>
<h3>Custom Seach-Depth</h3>
Custom-search depth, default is to show one level of child nodes.
<ul>
<li>acc++2</li>
<li>acc.a01.e01++3</li>
<li>infr.w02++2</li>
</ul>
All descendent nodes. Note this may take a little time for high-level ndoes.
<ul>
<li>acc.a03.a02*</li>
<li>acc.a04.a02*</li>
<li>acc.a05.a02*</li>
</ul>
No descendent nodes is also possible. In other words show only the parent/root node.
<ul>
<li>acc.e01$</li>
<li>acc.a02$</li>
<li>ts.cup$</li>
</ul>
<h3>Wildcard matching</h3>
<ul>
<li>acc.a01.e01*k01</li>
<li>acc.a04.a*.e*.e01.k01li>
<li>acc.a*.a02*.e01</li>
</ul>
<h3>Serialised queries</h3>
Comma-separated listing of queries can be processed to allow listing of items that are unrelated in the FBS tree structure. Any of the previously mentionted queries can be serialised.
<ul>
<li>acc,infr,ts,nss</li>
<li>acc.a01,acc.a02.*e01.k01$,acc.a03$,acc.a04$</li>
<li>infr.w02, nss.h01.beer</li>
</ul>
<h1>Additional Queries</h1>
<ul>
<li><b>Documents</b>: Lists documents attached to a given FBS node with possibility to view/download these documents. However, no support for detailed meta-data e.g. revision history, release status.</li>
<li><b>Cables</b>: Lists cables associated with FBS nodes</li>
<li><b>Devices</b>: Lists ESS Names associated with FBS nodes</li>
<li><b>EPICS PVs</b>: Lists EPICS PVs associated with FBS nodes</li>
</ul>
<p>The standard syntax is an FBS node. For example: =ESS.ACC.A01 is valid. All FBS nodes are upper-case so the case of the input node is optional. The leading =ESS is also optional.</p>
<p>For examples of more advanced usage see the <a href="https://en.wikipedia.org/wiki/Tooltip">tooltips</a> on the query buttons.</p>
<a href = "http://{{ server_addr }}/ess-viewer">Back to ESS-Viewer application</a>
