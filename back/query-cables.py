from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
from ics_cables import getCables

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/q_get_cables', methods=['POST'])
def q_get_cables():
    if request.method =='POST':
        data = request.get_json(force=True)
        details  = getCables(data['essName'], data['showEndpoints'], data['showLabels'], data['showOwners'])
        response = jsonify({'status': details['status'], 'cables': details['cables']})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True, port=5005)
