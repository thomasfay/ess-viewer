"""
Fetches ESS FBS from available JSON and XML sources for future consumption by web front-end.
"""
import urllib.request, sys
import json
import requests

url = "https://itip.esss.lu.se/chess/fbs.json"
response = requests.get(url).content.decode('utf-8')
installPath="/var/www/html/ess-viewer/backend/"

with open(installPath + 'fbs.json', 'w+') as inFile:
    inFile.write(response) 

url = "https://itip.esss.lu.se/chess/fbs.xml"
response = requests.get(url).content.decode('utf-8')
with open(installPath + 'fbs.xml', 'w+') as inFile:
    inFile.write(response)
