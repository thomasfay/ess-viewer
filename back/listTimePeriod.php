<?php
date_default_timezone_set('Europe/Stockholm');
$post = json_decode(file_get_contents('php://input'),true);
$nDays = $post['nDays'];
$incMeta = $post['incMeta'];
$path = "/var/www/html/ess-viewer/backend/calibTablesBackup/";
$dayToday = (int)date("d");
$monthToday = (int)date("m");
$yearToday = (int)date("Y");

$monthDays = array(31,28,31,30,31,30,31,31,30,31,31);
$fileListing=array();
if (is_dir($path)) {
  if($openpath = opendir($path)){
    while (($file = readdir($openpath)) !== false) {
      if ($file != '.' && $file != '..') {
        $timestamp = substr($file, strlen($file) - 23);
        $yearFile = (int)substr($timestamp,0,4);
        $monthFile = (int)substr($timestamp,5,2);
        $dayFile = (int)substr($timestamp,8,2);
        $i = $monthFile;
        $iDays = 0;
        if ($monthFile === $monthToday and $yearToday === $yearFile) {
          $iDays = $iDays + $dayToday - $dayFile;
        } else {
          while ($i <= $monthToday) {
            
            if ($i === ($monthToday - 1)) {
              $iDays = $monthDays[$i-1] - $dayFile;
            } elseif ($i == $monthToday) {
              $iDays = $iDays + $dayToday;
            } elseif ($i !== $monthToday) {
              $iDays = $iDays + (int)$monthDays[$i-1];
            }
            $i++; 
          }
        }
        
        if ($iDays <= $nDays) {
          if ($incMeta === true) {
            $meta = ", <b>modification time</b>: " .date("F d Y H:i:s.", filemtime($path . $file));
          }
          array_push($fileListing,$file . $meta);
        }
      }
    }
  }
}
echo json_encode($fileListing);
?>
