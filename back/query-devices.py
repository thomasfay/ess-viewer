from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
from ics_devices import getDevices

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/q_get_devices', methods=['POST'])
def q_get_pv_details():
    if request.method =='POST':
        data = request.get_json(force=True)
        devices = getDevices(data['pattern'], data['withDevices'], data['includeDesc'], data['includeName'], data['showActive'], data['includeStatus'])
        response = jsonify({'status': devices['status'], 'devices': devices['devices']})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
       
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True, port=5004)
