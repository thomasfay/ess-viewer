import os.path, time
import json
from datetime import datetime, timedelta
def get_sync_info():
    pathJSON='/var/www/html/ess-viewer/backend/fbs.json'
    pathXML='/var/www/html/ess-viewer/backend/fbs.xml'
    dictMonths = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}
    syncTimeJSON = time.ctime(os.path.getmtime(pathJSON))
    syncTimeXML = time.ctime(os.path.getmtime(pathXML))
    minSync = min(syncTimeJSON, syncTimeXML)
    intYear = int(minSync[-4:])
    month = minSync[4:7]
    intMonth = dictMonths[month]
    day = minSync[0:4]
    intDay = int(minSync[8:10])
    intHour = int(minSync[11:13])
    intMinutes = int(minSync[14:16])
    formattedTimestamp = day + month + ' ' + str(intDay) + ', ' + str(intHour) + ':' + str(intMinutes)
    dt = datetime(intYear,intMonth,intDay)
    lastWeek = datetime.now()
    lastWeek = lastWeek - timedelta(days = 7)
    with open(pathJSON, 'r') as jsonFBS:
        listBreakdown = json.load(jsonFBS)
    nNodes = len(listBreakdown)
    if dt < lastWeek:
        return "FBS source not synchronised for over one week" 
    else:
        return "FBS source last synchronised: " + formattedTimestamp + "; Node count: " + str(nNodes)
