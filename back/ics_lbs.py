import requests
import xml.etree.ElementTree as ET

def getLocation(essName, incName, incDesc, incIso):
    # Query FBS.xml based on ESS Name
    #IT integration platform
    url_itip = "https://itip.esss.lu.se/chess/fbs.xml"
    query_name = "?ESSName=" + essName
    response = requests.get(url_itip + query_name)
    root = ET.fromstring(response.content)
    l0 = root.getchildren()
    fbs = l0[0].attrib
    fbsTag = fbs['tag']
    name = ""
    iso = ""
    desc = ""
    if incName:
        name = ", <b>essName:</b> " + essName
    if incDesc:
        desc = ", <b>description:</b> " + fbs['description']
    if incIso:
        iso = ", <b>ISO Classification:</b> " + fbs['isoClass']
        
    locatedIn = l0[0].getchildren()
    lbs = locatedIn[0].attrib['tag']
    return fbsTag + ", " + lbs + name + desc + iso
     
    
