<?php
    date_default_timezone_set("Europe/Stockholm");
    $destination_path = getcwd().DIRECTORY_SEPARATOR."calibTables".DIRECTORY_SEPARATOR;
    $backup_path = getcwd().DIRECTORY_SEPARATOR."calibTablesBackup".DIRECTORY_SEPARATOR;
    $nFiles = count($_FILES);
    $date = date("Y-m-d_G-i-s");
    $jsonData = json_encode("Failed to upload ". $fileName);
    $cntUpload = 0;
    for ($i =0; $i < $nFiles; $i++) {
      $fileName = $_FILES['file'.$i]['name'];
      $tmpName = $_FILES['file'.$i]['tmp_name'];
      $target_path = $destination_path.$fileName;
      $timestamp_copy = str_replace('.csv', '_' . $date . '.csv', $backup_path. $fileName);
      $testSpace = substr_count($fileName, " ") === 0;
      $testColon = substr_count($fileName, ":") === 0;
      $testUnderscore = substr_count($fileName, "_") >= 2;
      if ($testColon && $testSpace && $testUnderscore) {
        if (@move_uploaded_file($tmpName, $target_path)) {
          if (copy($target_path, $timestamp_copy)) {
            $cntUpload = $cntUpload + 1;
          }
        }
      }
    }
    if ($cntUpload === 0) {
      $jsonData = json_encode('No files uploaded. Check file naming.');
    } else if ($cntUpload === 1) {
      $jsonData = json_encode('One file successfully uploaded.');
    } else if ($cntUpload > 1) {
      $jsonData = json_encode($cntUpload . ' files successfully uploaded.');
    }
    echo ($jsonData);
?>
