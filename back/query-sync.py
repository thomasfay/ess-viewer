from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
from sync_status import get_sync_info

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/q_get_sync_info', methods=['GET'])
def q_sync_info():
    if request.method == 'GET':
        sync_info = get_sync_info()
        response = jsonify({'sync_info': sync_info})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
        
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True, port=5003)
