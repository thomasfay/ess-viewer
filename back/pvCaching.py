# Cache PV listing locally for performance considerations
from ics import getDeviceListing
import urllib3
from channelfinder import ChannelFinderClient
from epicsarchiver import ArchiverAppliance


urllib3.disable_warnings()
syncPath = "/var/www/html/ess-viewer/backend/"
listPVs = list()
listArc = list()

url_cf = "https://channelfinder.tn.esss.lu.se"
cf = ChannelFinderClient(BaseURL = url_cf) 
archiver = ArchiverAppliance("archiver-01.tn.esss.lu.se")   

split = {"A01": "RFQ", "A02": "DTL", "W02": "MEBT", "A03": "Spoke", "A04": "MBL", "A05": "HBL", "A06": "TS2", "B01.B01": "PBI-BCM","B01.B02": "PBI-BLM", "B01.B03": "PBI-WS", "B01.B04": "PBI-FC", "B01.B05": "PBI-BPM" ,"G01": "Vacuum"}

# Then populate a list of all PVs belonging to the device
for el in split:
    listPVs = []
    listArc = []
    Devices = getDeviceListing("=ESS.ACC." + el)
    for device in Devices:
        channels = cf.find(name = device + ":*")
        for ch in channels:
            for prop in ch['properties']:
                if prop['name'] == 'pvStatus':
                    active = prop['value'] == 'Active'
                    break
            pv = ch['name']
            listPVs.append(pv)
            status = archiver.get_pv_status(pv)
            if status[0]['status'] == "Being archived":
                listArc.append(pv)
    with open(syncPath + "pvs" + split[el], 'w') as outFile:
        for pv in listPVs:
            outFile.write(pv + "\n")
    with open(syncPath + "arc" + split[el], 'w') as outFile:
        for pv in listArc:
            outFile.write(pv + "\n")
