<?php
$file = json_decode(file_get_contents('php://input'),true);
$fileName = $file['fileName'];
$date_regex = "/202[0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]_[0-9][0-9]_[0-9][0-9]/";
$path = "/var/www/html/ess-viewer/backend/calibTables/";
if (preg_match($date_regex, $fileName)) {
    $path = "/var/www/html/ess-viewer/backend/calibTablesBackup/";
}
$fileName = $path . $fileName;
$csvFile = file($fileName);
$data = [];
foreach ($csvFile as $line) {
    $data[] = str_getcsv($line);
}
echo json_encode($data);
?>
