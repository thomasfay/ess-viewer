<?php
$logDir="/var/www/html/ess-viewer/backend";
$fileContents = array();
if (is_dir($logDir)) {
  if ($openpath = opendir($logDir)){
    $i = 0;
    while(($file = readdir($openpath)) !== false) {
      if (strpos($file, "logBackup_") === 0) {
        $i++;
        $contents = "<b>Backup location #" . $i . "</b>\n";
        $fileHandle = file($logDir . "/" . $file);
        foreach($fileHandle as $line) {
          $contents = "<br>" . $contents . $line;
        } 
        array_push($fileContents, $contents);
      }
    }
  }
} 
echo json_encode($fileContents);
?>
