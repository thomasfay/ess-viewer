<?php
$filter = json_decode(file_get_contents('php://input'),true);
$incMeta = $filter['incMeta'];
$archived = $filter['archived'];
$filter = $filter['filter'];
if (strlen($archived) > 0) {
    $path = "/var/www/html/ess-viewer/backend/calibTablesBackup/";
} else {
    $path = "/var/www/html/ess-viewer/backend/calibTables/";
}
$fileListing = array();
date_default_timezone_set("Europe/Stockholm");
$meta = "";
if (is_dir($path)) {
  if($openpath = opendir($path)){
    while (($file = readdir($openpath)) !== false) {
        if ($file !== '.' && $file !== '..') {
          if (strlen($filter) == 0 || strpos($file, $filter) !== FALSE) {
              if ($incMeta === TRUE) {
                $meta = ", <b>modification time</b>: " .date("F d Y H:i:s.", filemtime($path . $file));
              }
              if ($file[0] !== '.') {
                array_push($fileListing,$file . $meta);
              }
          }
        }
    }
    closedir($openpath);
  }
}
echo json_encode($fileListing);
?>
