from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
from ics_pvs import getDetails, findChannels

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/q_get_pv_details', methods=['GET'])
def q_get_pv_details():
    if request.method =='GET':
        details  = getDetails(request.args['pv'])
        
        response = jsonify({'status': details['status'], 'desc': details['DESC'], 'sys': details['SYS'], 'host': details['host'], \
        'archStat': details['archStat'], 'devType': details['devType'], 'contact': details['contact'], \
        'fieldMeta': details['fieldMeta']})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
       
@app.route('/q_find_channels', methods = {'POST'})
def q_find_channels():
    if request.method == 'POST':
        data = request.get_json()
        channels = findChannels(data['pattern'], data['incDesc'], data['incSys'], data['incCh'], data['incAll'], data['showInactive'], data['useLab'])
        response = jsonify({'status': channels['status'], 'channels': channels['matches']})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True, port=5001)
