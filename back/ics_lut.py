from epics import caget
from channelfinder import ChannelFinderClient
import urllib3
import numpy

def getLookupTable(PV, calibPoints):
    lut = {'vals': [], 'desc': '', 'egu': ''}
    llrf = "RFS-DIG-" in PV
    if len(PV) < 10:
        return lut
    if calibPoints is None or len(calibPoints) < 2:
        return lut

    desc = caget(PV + '.DESC')
    if desc is not None:
        lut['desc'] = desc
    egu = caget(PV + '.EGU')
    if egu is not None:
        lut['egu'] = egu
    nord = caget(PV + '.NORD')
    try:
        nord = int(nord)
    except ValueError:
        nord = 65536
    if nord <2:
        vals = []
        for i in range(0,len(calibPoints)):
            vals.append(0)
        lut['vals'] = vals
        return lut

    vals = caget(PV, count = nord )
    valsReduced = []
    if len(vals) > 0:
        # Handle non-numerical end value gracefully 
        if numpy.isnan(vals[-1]):
            vals[-1] = vals[-2]
        
        for i,pnt in enumerate(calibPoints):
            if llrf:
                valsReduced.append(vals[i])
            else:
                valsReduced.append(vals[pnt])
        lut['vals'] = valsReduced
        return lut
    else:
        lut['vals'] = "Failed to retrieve lookup table."
        return lut

def translateToBasePV(file):
    if "RFS-DIG-" in file:
        # LLRF 
        basePV = file.replace('_',':').replace('.csv','')
    else:
        # RF LPS
        basePV = file.replace('_',':').replace('.csv','-Desc')
    return basePV

def getLutPV(file):
    url_cf = "http://channelfinder.tn.esss.lu.se"
    urllib3.disable_warnings()
    try:
        cf = ChannelFinderClient(BaseURL=url_cf)
    except:
        details['status'] = "Could not connect to channel finder"    
        return False
    basePV = translateToBasePV(file) 
    res = cf.find(name=basePV)
    hw_channel = "test"
    pv = ""
    for prop in res[0]['properties']:
        suffix = ""
        if prop['name'] == "HW-Channel":
            hw_channel = prop['value']
            # Translate hardware information to LUT reference
            # RFLPS
            if "AI_" in hw_channel:
                ch_id = ""
                for char in hw_channel:
                    if char.isnumeric():
                        ch_id += char 
                pos1 = basePV.find("-")
                pos2 = basePV[pos1+1:].find("-")
                pos3 = basePV[pos1+pos2+2:].find("-")
                sysNum = basePV[pos1 + pos2 + pos3 + 3]
                if int(ch_id) < 10:
                    suffix = ":RFS-FIM-" + str(sysNum) + "01:RF" + str(int(ch_id) + 1)
                else:
                    suffix = ":RFS-FIM-" + str(sysNum) + "01:AI" + str(int(ch_id) - 0)
                # LUT property
                pos1 = basePV.find(":")
                pos2 = basePV.find(":")
                pv = basePV[:pos1] + suffix + "-Calib-LUT"
                break
    return pv 

def getLutPVs(listFiles):
    url_cf = "http://channelfinder.tn.esss.lu.se"
    urllib3.disable_warnings()
    try:
        cf = ChannelFinderClient(BaseURL=url_cf)
    except:
        details['status'] = "Could not connect to channel finder"    
        return False

    #First get base PV name (engineer property) then map to acquisition channel using "HW-Channel" info tag.
    list_basePVs = []
    for file in listFiles:
       list_basePVs.append(translateToBasePV(file))
   
    listPVs = []
    for pv in list_basePVs: 
        res = cf.find(name=pv)
        for el in res:
            hw_channel = "test"
            for prop in el['properties']:
                suffix = ""
                if prop['name'] == "HW-Channel":
                    hw_channel = prop['value']
                    # Translate hardware information to LUT reference
                    #RFLPS
                    if "AI_" in hw_channel:
                        ch_id = ""
                        for char in hw_channel:
                            if char.isnumeric():
                                ch_id += char

                        if int(ch_id) < 10:
                            suffix = "FIM-101:RF" + str(int(ch_id) + 1)
                        else:
                            suffix = "FIM-101:AI" + str(int(ch_id) - 9)
                        # LUT property
                        pos1 = pv.find("-")
                        pos2 = pv[pos1+1:].find("-")
                        pv = pv[:pos1 + pos2 + 2] 

                        pv = pv + suffix + "-Calib-LUT"
                        listPVs.append(pv)
                        break
                    #LLRF - no action for now

    listOutput = []
    for pv in listPVs: 
        res = cf.find(name=pv)
        for el in res:
            if not el['properties'][0]['value'] == "Inactive":
                listOutput.append(el['name'])
    
    return listOutput
