import json
import sys
import os
import requests
import subprocess
import re
import fnmatch
import time

def hyperlink_it(hostName):
    return '<a href="https://csentry.esss.lu.se/network/hosts/view/' + hostName + '">' + hostName + '</a><br>'

def get_hosts(query, withDesc, withType):
    # Initialisation
    hosts = {'status': 'init', 'hosts': ''}
     
    url_cs = 'https://csentry.esss.lu.se/api/v1'
    url_cs_view = 'https://csentry.esss.lu.se/network/hosts/view/'
    cs_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1OTEwMDY3NTEsIm5iZiI6MTU5MTAwNjc1MSwianRpIjoiMTBkYWM3Y2MtM2UzNi00ZjBhLWIyNTctMWU5YmE1NjczOTRkIiwiaWRlbnRpdHkiOjIwLCJmcmVzaCI6ZmFsc2UsInR5cGUiOiJhY2Nlc3MifQ.rKXbr66DIDdL58Gu20P07WXdI1JN8trJy15glI4UdEM'
    

    # Additional Name matching functionality
    if '&' in query:
        pos = query.find('&')
        nameMatch = query[pos+1:].lower()
        query = query[:pos]
    else:
        nameMatch = ''  
     
    # Query to CSEntry web-service
    cs_query = '/network/hosts/search?per_page=100&q=' + query
    lenPref = len("< Link: ") + 1
    curlArgs = ['/usr/bin/curl', '-v', '--header', 'Authorization: Bearer ' + cs_token, '--header', 'Connection: close'  ,  url_cs + cs_query]
    x = subprocess.run(curlArgs, capture_output = True) 
    jsonHosts = x.stdout
    jsonHosts = json.loads(jsonHosts)
    meta = x.stderr
    strLink = meta.decode('utf-8').split('\n')[25]
    remainingData = 'rel="next"' in strLink
    if remainingData:
        end = strLink.find('>; rel="next"')
        if end == -1:
            remainingData = False
        else:
            curlArgs[-1] = strLink[lenPref:end].replace("%3A",":")
    while remainingData:
        x = subprocess.run(curlArgs, capture_output = True)
        jsonTemp = x.stdout
        jsonTemp = json.loads(jsonTemp)
        jsonHosts.extend(jsonTemp)
        meta = x.stderr
        strFirst = meta.decode('utf-8').split('\n')[0]
        strLink = meta.decode('utf-8').split('\n')[25]
        remainingData = 'rel="next"' in strLink
        if remainingData:
            end = strLink.find('>; rel="next"')
            if end == -1:
                remainingData = False
                break
            else:
                if 'rel="prev"' in strLink:
                    start = strLink.find('rel="prev"') + len('rel="prev"') + 3
                else:
                    start = lenPref
                curlArgs[-1] = strLink[start:end].replace("%3A",":")
    iHosts = 0
    for el in jsonHosts:
        if len(nameMatch) == 0 or nameMatch in el['name']:
            iHosts += 1
            if withDesc:
                if el['description'] is not None:
                    desc = ", <b>'description'</b>: " + el['description']
                else:
                    desc = ", <b>'description'</b>: No description."
            else:
                desc = ""
            if withType and el['device_type'] is not None:
                devType = ", <b>'deviceType'</b>: " + el['device_type']
            else:
                devType = ""
            hosts['hosts'] += "<a href=" + url_cs_view  + el['name'] + ">" + str(iHosts) + ": " + el['name'] + "</a>" + desc + devType + "<br>"

    hosts['status'] = "Found " + str(hosts['hosts'].count("<br>")) + " hosts."

    if len(hosts['hosts']) > 4:
        if hosts['hosts'][-4:] == "<br>":
            hosts['hosts'] = hosts['hosts'][:-4]
    return hosts
