from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
from ics_lut import getLookupTable, getLutPVs, getLutPV

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/q_get_lookup_table', methods=['POST'])
def q_get_location():
    if request.method =='POST':
        data = request.get_json()
        lut = getLookupTable(data['PV'], data['calibPoints'])
        response = jsonify({'vals': lut['vals'], 'desc': lut['desc'], 'egu': lut['egu']})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/q_get_lookup_pv', methods=['GET'])
def q_get_lookup_pv():
    if request.method == "GET":
        file = request.args['file']
        pv = getLutPV(file)
        response = jsonify({'pv': pv})  
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/q_get_lookup_pvs', methods=['POST'])
def q_get_lookup_pvs():
    if request.method == 'POST':
        data = request.get_json()
        pvs = getLutPVs(data['calibFiles'])
        response = jsonify({'pvs': pvs})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True, port=5007)
